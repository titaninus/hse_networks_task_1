drop table if exists entries;
drop table if exists users;
create table entries (
  key text primary key not NULL ,
  value text not null
);

CREATE TABLE users (
  id INTEGER PRIMARY KEY autoincrement,
  username TEXT NOT NULL,
  password TEXT NOT NULL,
  salt BLOB NOT NULL,
  pass_type TEXT NOT NULL
);