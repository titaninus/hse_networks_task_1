from .webserver import socketio, application

if __name__ == "__main__":
    socketio.run(application)
