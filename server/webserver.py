import os
import sys
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash
from flask_scrypt import generate_random_salt, generate_password_hash, check_password_hash
from flask_socketio import SocketIO, emit
from subprocess import PIPE, Popen
from werkzeug.contrib.fixers import ProxyFix
import hashlib
import random


application = Flask(__name__)
application.config.from_object(__name__)
socketio = SocketIO(application)

application.wsgi_app = ProxyFix(application.wsgi_app)
application.config.update(dict(
    DATABASE='/vagrant/server/database.db',
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
application.config.from_envvar('HSE_NETWORKS_SETTINGS', silent=True)


def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@application.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


def connect_db():
    """Connects to the specific database."""
    #rv = sqlite3.connect(application.config['DATABASE'])
    rv = sqlite3.connect("/vagrant/server/database.db")
    rv.row_factory = sqlite3.Row
    return rv


def init_db():
    db = get_db()
    with application.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()


def init_db_raw():
    db = connect_db()
    with application.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()


@application.route('/')
def hello_world():
    return redirect(url_for("show_entries"))


@application.route('/store')
def show_entries():
    db = get_db()
    cur = db.execute('select key, value from entries')
    entries = cur.fetchall()
    return render_template('show_entries.html', entries=entries)


@application.route('/store/add', methods=['POST'])
def add_entry():
    if not session.get('logged_in'):
        abort(401)
    db = get_db()
    db.execute('insert into entries (key, value) values (?, ?)',
                 [request.form['key'], request.form['value']])
    db.commit()
    flash('New entry was successfully posted')
    session.hasresult = False
    return redirect(url_for('show_entries'))


@application.route('/store/raw_add', methods=['POST'])
def raw_entry():
    session.get('logged_in')
    # if not session.get('logged_in'):
    #     abort(401)
    db = get_db()
    cur = db.execute("select * from users where id = %d" % random.randint(0, 500)) #request.form["query"])
    res = cur.fetchall()
    strings = []
    for i in res:
        stringres = ""
        for j in i.keys():
            stringres += "   " + j + ": " + str(i[j])
        strings.append(stringres)
    db.commit()
    session.hasresult = True
    #return redirect(url_for('show_entries'))
    return render_template("show_entries.html", result=strings)


@application.route('/db', methods=["GET", "POST"])
def db():
    if request.method == "POST":
        return raw_entry()
    else:
        return show_entries()


def check_password(type, passw, salt, origin):
    if type == "sha256":
        #print(passw, salt, hashlib.sha256(hashlib.sha256(bytes(passw, "utf-8")).digest() + salt).digest(), origin)
        #tmp = hashlib.sha256(str(hashlib.sha256(str(passw)).digest() + str(salt)))
        return hashlib.sha256(hashlib.sha256(bytes(passw, "utf-8")).digest() + salt).digest() == origin
    else:
        return check_password_hash(passw, origin, salt)


@application.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] == application.config['USERNAME']:
            if request.form['pass'] != application.config['PASSWORD']:
                error = 'Invalid password'
            else:
                session['logged_in'] = True
                flash('You were logged in')
                return redirect(url_for('show_entries'))
        else:
            db = get_db()
            cur = db.execute('select username, password, salt, pass_type from users where username = ?', [request.form['username']])
            data = cur.fetchone()
            if data:
                if check_password(data[3], request.form['pass'], data[2], data[1]):
                #if data[1] == request.form['password']:
                    session['logged_in'] = True
                    flash('You were logged in')
                    return redirect(url_for('show_entries'))
                else:
                    error = 'Invalid password'
            else:
                error = 'Invalid username'
    return render_template('login.html', error=error)


@application.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))




@application.route('/singup', methods=["GET", "POST"])
def singup():
    error = None
    if request.method == "POST":
        user = request.form['username']
        password = request.form['pass']
        db = get_db()
        cur = db.execute('select username from users where username = ?', [user])
        data = cur.fetchone()
        if not data:
            salt = generate_random_salt()
            password_hash = generate_password_hash(password, salt)
            db.execute('insert into users (username, password, salt, pass_type) values (?, ?, ?, ?)',
                 [user, password_hash, salt, "scrypt"])
            db.commit()
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
        else:
            error = 'User with this name is already exists'
    return render_template('singup.html', error=error)





@application.route('/singup_sha', methods=["GET", "POST"])
def singup_sha():
    error = None
    if request.method == "POST":
        user = request.form['username']
        password = request.form['pass']
        db = get_db()
        cur = db.execute('select username from users where username = ?', [user])
        data = cur.fetchone()
        if not data:
            salt = generate_random_salt()
            password_hash = hashlib.sha256(hashlib.sha256(bytes(password, "utf-8")).digest() + salt).digest()
            db.execute('insert into users (username, password, salt, pass_type) values (?, ?, ?, ?)',
                 [user, password_hash, salt, "sha256"])
            db.commit()
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
        else:
            error = 'User with this name is already exists'
    return render_template('singup.html', error=error)


@application.route('/registration', methods=["GET", 'POST'])
def reg():
    return singup_sha()

@application.route("/ping")
def ping():
    return render_template('ping.html')


@socketio.on('ping', namespace='/pingsc')
def test_message(message):
    p = Popen(['ping ' + message["host"] + " " + message["params"]], shell=True, stdout=PIPE)
    for line in p.stdout:
        emit('pingData', {'data': str(line)})


@socketio.on('connect', namespace='/pingsc')
def test_connect():
    emit('pingData', {'data': 'Connected to server'})


@socketio.on('disconnect', namespace='/pingsc')
def test_disconnect():
    print('Client disconnected')


def factor(n):
    Ans = []
    d = 2
    while d * d <= n:
        if n % d == 0:
            Ans.append(d)
            n //= d
        else:
            d += 1
    if n > 1:
        Ans.append(n)
    return Ans


@application.route("/calc", methods=["GET", "POST"])
def factorize():
    if request.method == "POST":
        #number = request.form['number']
        number = 110113
        ans = factor(int(number))
        return render_template('factor.html', ans=" ".join(list(map(str, ans))))
    return render_template('factor.html', ans=None)


if __name__ == '__main__':
    if "initdb" in sys.argv:
        print("InitDb")
        init_db_raw()
    else:
        socketio.run(application)


@application.cli.command('initdb')
def initdb_command():
    """Initializes the database."""
    init_db()
    print('Initialized the database.')

